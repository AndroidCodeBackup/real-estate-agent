package idsoft.realtor;

import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SendDiyApk extends Activity{
	EditText etto,etbody,etsubject;
	CheckBox cbattachreport;
	LinearLayout llreport;
	CommonFunctions cf;
	ShowToast toast;
	String to,subject,body,address,agentid,name;
	int show_handler;
	String classidentifier,version;
	TextView tvpercentage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.senddiyapk);
		cf=new CommonFunctions(this);
		etto=(EditText)findViewById(R.id.senddiyapk_etto);
		etsubject=(EditText)findViewById(R.id.senddiyapk_etsubject);
		etbody=(EditText)findViewById(R.id.senddiyapk_etbody);
		llreport=(LinearLayout)findViewById(R.id.senddiyapk_llreport);
		cbattachreport=(CheckBox)findViewById(R.id.senddiyapk_ckattachreport);
		tvpercentage=(TextView)findViewById(R.id.senddiyapk_tvpercentage);
		etto.setText(address);
		etto.addTextChangedListener(new CustomTextWatcher(etto));
//		etbody.addTextChangedListener(new CustomTextWatcher(etbody));
		etsubject.addTextChangedListener(new CustomTextWatcher(etsubject));
		
		etbody.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etbody.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etbody.setText("");
		        }
				int length=etbody.getText().toString().length();
				if(length==0)
				{
					tvpercentage.setText("0%");
				}
				else
				{
					length=length/3;
					tvpercentage.setText(String.valueOf(length)+"%");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// get application version
				 try {
						
					 version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 //get application version
		cf.Device_Information();
		
	}
	
	public void clicker(View v) {
		switch(v.getId())
		{
		case R.id.senddiyapk_placeorder:
			Intent placeintent=new Intent(SendDiyApk.this,OrderInspection.class);
			startActivity(placeintent);
			finish();
			break;
		
		case R.id.senddiyapk_home:
			Intent intent=new Intent(SendDiyApk.this,HomeScreen.class);
			startActivity(intent);
			finish();
			break;
			
		case R.id.senddiyapk_btnsend:
			check_validation();
			break;
			
		case R.id.senddiyapk_btncancel:
			Intent intentback=new Intent(SendDiyApk.this,HomeScreen.class);
			startActivity(intentback);
			finish();
			break;
		}
	}
	
	private void check_validation()
	{
		if(!etto.getText().toString().trim().equals(""))
		{
			if(cf.eMailValidation(etto.getText().toString().trim()))
			{
			if(!etsubject.getText().toString().trim().equals(""))
			{
				if(!etbody.getText().toString().trim().equals(""))
				{
					send_mail();
				}
				else
				{
					toast = new ShowToast(SendDiyApk.this,
							"Please enter comments");
					etbody.setText("");
					etbody.requestFocus();
				}
			}
			else
			{
				toast = new ShowToast(SendDiyApk.this,
						"Please enter subject");
				etsubject.setText("");
				etsubject.requestFocus();
			}
			}
			else
			{
				toast = new ShowToast(SendDiyApk.this,
						"Please enter a valid email address");
				etto.setText("");
				etto.requestFocus();
			}
		}
		else
		{
			toast = new ShowToast(SendDiyApk.this,
					"Please enter to address");
			etto.setText("");
			etto.requestFocus();
		}
	}
	
	private void send_mail()
	{
		cf.CreateTable(1);
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			agentid = cf
					.decode(cur1.getString(cur1.getColumnIndex("agentid")));
		}
		cur1.close();
		
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Processing... Please wait.");
				new Thread() {
					String chklogin;
					public void run() {
						Looper.prepare();
						try {
							chklogin = cf
									.Calling_WS_SendApk(agentid,etto.getText().toString(),etsubject.getText().toString(),
											etbody.getText().toString(),"SENDAPKMAIL");
							System.out.println("response SENDAPKMAIL"
									+ chklogin);
							show_handler = 5;
							handler.sendEmptyMessage(0);
						} catch (SocketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (NetworkErrorException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (TimeoutException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 4;
							handler.sendEmptyMessage(0);

						}

					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								toast = new ShowToast(
										SendDiyApk.this,
										"There is a problem on your Network. Please try again later with better Network.");

							} else if (show_handler == 4) {
								show_handler = 0;
								toast = new ShowToast(
										SendDiyApk.this,
										"There is a problem on your application. Please contact Paperless administrator.");

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(chklogin.toLowerCase().equals("true"))
								{
									Intent intenthome = new Intent(SendDiyApk.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									toast = new ShowToast(
											SendDiyApk.this,
											"DIY Application sent successfully.");
								}
								else
								{
									Intent intenthome = new Intent(SendDiyApk.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									toast = new ShowToast(
											SendDiyApk.this,
											"There is a problem on your application. Please contact Paperless administrator.");
								}
								
							}
						}
					};
				}.start();
			} else {
				toast = new ShowToast(SendDiyApk.this,
						"Internet connection not available");
			}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(SendDiyApk.this,HomeScreen.class);
		startActivity(intent);
		finish();
	}
}
