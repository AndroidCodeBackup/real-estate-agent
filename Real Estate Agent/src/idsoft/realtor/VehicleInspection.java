package idsoft.realtor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class VehicleInspection extends Activity {
	RadioGroup rbpaintcondition, rbdents, rbrustproblems,
			rbappeartobeinaccident, rbdamagetotheframe, rbdamagetowindshield,
			rbanyfluidleaks, rbengineoil, rbtransmissionfluid, rbdifferential,
			rbcoolant, rbbrakefluid, rbpowersteeringfluid, rbconditionoftires,
			rbconditionofvalvestems, rbcaster, rbanyproblemwithsuspension,
			rbanyproblemwithalignment, rbujoints, rbcvboots, rbshocks,
			rbballjoints, rbbushings, rblinkpins, rbrockandpinion, rbidler,
			rbcenterlink, rbexhaustsystemcomplete, rbemissioncontrolintact,
			rbdoallbelts, rbconditionofsparkplugs, rbdothebrakesgrab,
			rbdoescarshudder, rbarebrakelinings, rbconditionofbrakepads,
			rbanyproblemswithhudraulicsystem, rbdoesparkingbrake,
			rbisbrakepedalpressureokay, rbmastercylinder, rbdrumsanddiscs,
			rbwheelcylinder, rbcalipters, rbinstruments, rbinteriorlights,
			rbheadlights, rbtaillights, rbturnsignals, rbbackuplights,
			rbbrakelightsrbemergencylights, rbsystemfailure,
			rbairconditioningsystem, rbheatingsystem, rbconditionofbattery,
			rbdoorlocks, rbdoallpoweroptionswork, rbanydelaybetweenenginespeed,
			rbautomatictransmission;
	String strpaintcondition, strdents, strrustproblems,
			strappeartobeinaccident, strdamagetotheframe,
			strdamagetowindshield, stranyfluidleaks, strengineoil,
			strtransmissionfluid, strdifferential, strcoolant, strbrakefluid,
			strpowersteeringfluid, strconditionoftires,
			strconditionofvalvestems, strcaster, stranyproblemwithsuspension,
			stranyproblemwithalignment, strujoints, strcvboots, strshocks,
			strballjoints, strbushings, strlinkpins, strrockandpinion,
			stridler, strcenterlink, strexhaustsystemcomplete,
			stremissioncontrolintact, strdoallbelts, strconditionofsparkplugs,
			strdothebrakesgrab, strdoescarshudder, strarebrakelinings,
			strconditionofbrakepads, stranyproblemswithhudraulicsystem,
			strdoesparkingbrake, strisbrakepedalpressureokay,
			strmastercylinder, strdrumsanddiscs, strwheelcylinder,
			strcalipters, strinstruments, strinteriorlights, strheadlights,
			strtaillights, strturnsignals, strbackuplights,
			strbrakelightsrbemergencylights, strsystemfailure,
			strairconditioningsystem, strheatingsystem, strconditionofbattery,
			strdoorlocks, strdoallpoweroptionswork,
			stranydelaybetweenenginespeed, strautomatictransmission;
	LinearLayout llgeneralinfoborder, llbodyandframe, llfluidlevels,
			llsuspension, llengineperformance, llbrakesystems,
			lloperationalaccessories, lladdaimage, llcoverpagelogo;
	ImageView plus, minus, plus1, minus1, plus3, minus3, plus4, minus4, plus5,
			minus5, plus6, minus6, plus7, minus7, plus8, minus8, plus9, minus9;
	RadioButton rbowner, rbrepresentative, rbagent, rbwhowaspresentother,
			rbcar, rbbus, rbmotor, rbtruck, rbvan, rbvehicletypeother;
	EditText etclientname, etaddress, etdateofinspection, etinsurancecompany,
			etvehicleno, etvin, etlicenseno, etexpires, etmodel, etmake;
	EditText otherpresentatinspection, othervehicletype, otherpaintcondition,
			otherdents, otherrustproblems, otherengineoil,
			othertransmissionfluid, otherdifferential, othercoolant,
			otherbrakefluid, otherpowersteeringfluid, othercaster,
			otherujoints, othercvboots, othershocks, otherballjoints,
			otherbushings, otherlinkpins, otherrockandpinion, otheridler,
			othercenterlink, otherexhaustsystem, otheremissioncontrol,
			othermastercylinder, otherdrumsanddiscs, otherwheelcylinders,
			othercalipters, otherinstruments, otherinteriorlights,
			otherheadlights, othertaillights, otherturnsignals,
			otherbackuplights, otherbrakelights, otheremergencylights,
			othersystemfailurewarninglights, otherairconditioningsystem,
			otherheatingsystem, otherdoorlocks;
	LinearLayout llpresentatinspection, llvehicletype, llpaintcondition,
			lldents, llrustproblems, llengineoil, lltransmissionfluid,
			lldifferential, llcoolant, llbrakefluid, llpowersteeringfluid,
			llcaster, llujoints, llcvboots, llshocks, llballjoints, llbushings,
			lllinkpins, llrockandpinion, llidler, llcenterlink,
			llexhaustsystem, llemissioncontrol, llmastercylinder,
			lldrumsanddiscs, llwheelcylinders, llcalipters, llinstruments,
			llinteriorlights, llheadlights, lltaillights, llturnsignals,
			llbackuplights, llbrakelights, llemergencylights,
			llsystemfailurewarninglights, llairconditioningsystem,
			llheatingsystem, lldoorlocks;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vehicle_inspection);
		llgeneralinfoborder = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutgeneralinfoborder);
		llbodyandframe = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutbodyandframe);
		llfluidlevels = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutfluidlevels);
		llsuspension = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutsuapension);
		llengineperformance = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutengineperformance);
		llbrakesystems = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutbrakesystems);
		lloperationalaccessories = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutoperationalaccessories);
		lladdaimage = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutaddaimage);
		llcoverpagelogo = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutcoverpagelogo);
		llpresentatinspection = (LinearLayout) findViewById(R.id.vehicleinspection_llpresentatinspection);
		llvehicletype = (LinearLayout) findViewById(R.id.vehicleinspection_llvehicetypeother);
		llpaintcondition = (LinearLayout) findViewById(R.id.vehicleinspection_llpaintconditionother);
		lldents = (LinearLayout) findViewById(R.id.vehicleinspection_lldentsother);
		llrustproblems = (LinearLayout) findViewById(R.id.vehicleinspection_llrustproblemother);
		llengineoil = (LinearLayout) findViewById(R.id.vehicleinspection_llengineoilother);
		lltransmissionfluid = (LinearLayout) findViewById(R.id.vehicleinspection_lltransmissionfluidother);
		lldifferential = (LinearLayout) findViewById(R.id.vehicleinspection_lldifferentialother);
		llcoolant = (LinearLayout) findViewById(R.id.vehicleinspection_llcoolantother);
		llbrakefluid = (LinearLayout) findViewById(R.id.vehicleinspection_llbrakefluidother);
		llpowersteeringfluid = (LinearLayout) findViewById(R.id.vehicleinspection_llpowersteeringfluidother);
		llcaster = (LinearLayout) findViewById(R.id.vehicleinspection_llcasterother);
		llujoints = (LinearLayout) findViewById(R.id.vehicleinspection_llujointsother);
		llcvboots = (LinearLayout) findViewById(R.id.vehicleinspection_llcvbootsother);
		llshocks = (LinearLayout) findViewById(R.id.vehicleinspection_llshocksother);
		llballjoints = (LinearLayout) findViewById(R.id.vehicleinspection_llballjointsother);
		llbushings = (LinearLayout) findViewById(R.id.vehicleinspection_llbushingsother);
		lllinkpins = (LinearLayout) findViewById(R.id.vehicleinspection_lllinkpinspther);
		llrockandpinion = (LinearLayout) findViewById(R.id.vehicleinspection_llrockandpinionother);
		llidler = (LinearLayout) findViewById(R.id.vehicleinspection_llidlerother);
		llcenterlink = (LinearLayout) findViewById(R.id.vehicleinspection_llcenterlinkother);
		llexhaustsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llexhaustsystemother);
		llemissioncontrol = (LinearLayout) findViewById(R.id.vehicleinspection_llemissioncontrolother);
		llmastercylinder = (LinearLayout) findViewById(R.id.vehicleinspection_llmastercylinderother);
		lldrumsanddiscs = (LinearLayout) findViewById(R.id.vehicleinspection_lldrumsanddiscsother);
		llwheelcylinders = (LinearLayout) findViewById(R.id.vehicleinspection_llwheelcylinderdrumbrakeother);
		llcalipters = (LinearLayout) findViewById(R.id.vehicleinspection_llcalipersother);
		llinstruments = (LinearLayout) findViewById(R.id.vehicleinspection_llinstrumentsother);
		llinteriorlights = (LinearLayout) findViewById(R.id.vehicleinspection_llinteriorlightsother);
		llheadlights = (LinearLayout) findViewById(R.id.vehicleinspection_llheadlightsother);
		lltaillights = (LinearLayout) findViewById(R.id.vehicleinspection_lltaillightsother);
		llturnsignals = (LinearLayout) findViewById(R.id.vehicleinspection_llturnsignalsother);
		llbackuplights = (LinearLayout) findViewById(R.id.vehicleinspection_llbackuplightsother);
		llbrakelights = (LinearLayout) findViewById(R.id.vehicleinspection_llbrakefluidother);
		llemergencylights = (LinearLayout) findViewById(R.id.vehicleinspection_llemergencyother);
		llsystemfailurewarninglights = (LinearLayout) findViewById(R.id.vehicleinspection_llsystemfailureother);
		llairconditioningsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llairconditioningother);
		llheatingsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llheatingsystemother);
		lldoorlocks = (LinearLayout) findViewById(R.id.vehicleinspection_lldoorlocksother);

		otherpresentatinspection = (EditText) findViewById(R.id.vehicleinspection_etpresentatinspectionother);
		othervehicletype = (EditText) findViewById(R.id.vehicleinspection_etvehicletypeother);
		otherpaintcondition = (EditText) findViewById(R.id.vehicleinspection_etpaintconditionother);
		otherdents = (EditText) findViewById(R.id.vehicleinspection_etdentsother);
		otherrustproblems = (EditText) findViewById(R.id.vehicleinspection_etrustproblemother);
		otherengineoil = (EditText) findViewById(R.id.vehicleinspection_etengineoilother);
		othertransmissionfluid = (EditText) findViewById(R.id.vehicleinspection_ettransmissionfluidother);
		otherdifferential = (EditText) findViewById(R.id.vehicleinspection_etdifferentialother);
		othercoolant = (EditText) findViewById(R.id.vehicleinspection_etcoolantother);
		otherbrakefluid = (EditText) findViewById(R.id.vehicleinspection_etbrakefluidother);
		otherpowersteeringfluid = (EditText) findViewById(R.id.vehicleinspection_etpowersteeringfluidother);
		othercaster = (EditText) findViewById(R.id.vehicleinspection_etcasterother);
		otherujoints = (EditText) findViewById(R.id.vehicleinspection_etujointsother);
		othercvboots = (EditText) findViewById(R.id.vehicleinspection_etcvbootsother);
		othershocks = (EditText) findViewById(R.id.vehicleinspection_etshocksother);
		otherballjoints = (EditText) findViewById(R.id.vehicleinspection_etballjointsother);
		otherbushings = (EditText) findViewById(R.id.vehicleinspection_etbushingsother);
		otherlinkpins = (EditText) findViewById(R.id.vehicleinspection_etlinkpinsother);
		otherrockandpinion = (EditText) findViewById(R.id.vehicleinspection_etrockandpinionother);
		otheridler = (EditText) findViewById(R.id.vehicleinspection_etidlerother);
		othercenterlink = (EditText) findViewById(R.id.vehicleinspection_etcenterlinkother);
		otherexhaustsystem = (EditText) findViewById(R.id.vehicleinspection_etexhaustsystemother);
		otheremissioncontrol = (EditText) findViewById(R.id.vehicleinspection_etemissioncontrolother);
		othermastercylinder = (EditText) findViewById(R.id.vehicleinspection_etmastercylinderother);
		otherdrumsanddiscs = (EditText) findViewById(R.id.vehicleinspection_etdrumsanddiscsother);
		otherwheelcylinders = (EditText) findViewById(R.id.vehicleinspection_etwheelcylinderdrumbrakeother);
		othercalipters = (EditText) findViewById(R.id.vehicleinspection_etcalipersother);
		otherinstruments = (EditText) findViewById(R.id.vehicleinspection_etinstrumentsother);
		otherinteriorlights = (EditText) findViewById(R.id.vehicleinspection_etinteriorlightsother);
		otherheadlights = (EditText) findViewById(R.id.vehicleinspection_etheadlightsother);
		othertaillights = (EditText) findViewById(R.id.vehicleinspection_ettaillightsother);
		otherturnsignals = (EditText) findViewById(R.id.vehicleinspection_etturnsignalsother);
		otherbackuplights = (EditText) findViewById(R.id.vehicleinspection_etbackuplightsother);
		otherbrakelights = (EditText) findViewById(R.id.vehicleinspection_etbrakefluidother);
		otheremergencylights = (EditText) findViewById(R.id.vehicleinspection_etemergencyother);
		othersystemfailurewarninglights = (EditText) findViewById(R.id.vehicleinspection_etsystemfailureother);
		otherairconditioningsystem = (EditText) findViewById(R.id.vehicleinspection_etairconditioningother);
		otherheatingsystem = (EditText) findViewById(R.id.vehicleinspection_etheatingsystemother);
		otherdoorlocks = (EditText) findViewById(R.id.vehicleinspection_etdoorlocksother);

		plus = (ImageView) findViewById(R.id.vehicleinspection_plus);
		minus = (ImageView) findViewById(R.id.vehicleinspection_minus);
		plus1 = (ImageView) findViewById(R.id.vehicleinspection_plus1);
		minus1 = (ImageView) findViewById(R.id.vehicleinspection_minus1);
		plus3 = (ImageView) findViewById(R.id.vehicleinspection_plus3);
		minus3 = (ImageView) findViewById(R.id.vehicleinspection_minus3);
		plus4 = (ImageView) findViewById(R.id.vehicleinspection_plus4);
		minus4 = (ImageView) findViewById(R.id.vehicleinspection_minus4);
		plus5 = (ImageView) findViewById(R.id.vehicleinspection_plus5);
		minus5 = (ImageView) findViewById(R.id.vehicleinspection_minus5);
		plus6 = (ImageView) findViewById(R.id.vehicleinspection_plus6);
		minus6 = (ImageView) findViewById(R.id.vehicleinspection_minus6);
		plus7 = (ImageView) findViewById(R.id.vehicleinspection_plus7);
		minus7 = (ImageView) findViewById(R.id.vehicleinspection_minus7);
		plus8 = (ImageView) findViewById(R.id.vehicleinspection_plus8);
		minus8 = (ImageView) findViewById(R.id.vehicleinspection_minus8);
		plus9 = (ImageView) findViewById(R.id.vehicleinspection_plus9);
		minus9 = (ImageView) findViewById(R.id.vehicleinspection_minus9);
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.vehicleinspection_home:
			Intent intenthome = new Intent(VehicleInspection.this,
					HomeScreen.class);
			startActivity(intenthome);
			finish();
			break;

		case R.id.vehicleinspection_submit:
			check_validation();
			break;

		case R.id.vehicleinspection_plus:
			llgeneralinfoborder.setVisibility(View.VISIBLE);
			plus.setVisibility(View.GONE);
			minus.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus:
			llgeneralinfoborder.setVisibility(View.GONE);
			plus.setVisibility(View.VISIBLE);
			minus.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus1:
			llbodyandframe.setVisibility(View.VISIBLE);
			plus1.setVisibility(View.GONE);
			minus1.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus1:
			llbodyandframe.setVisibility(View.GONE);
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus3:
			llfluidlevels.setVisibility(View.VISIBLE);
			plus3.setVisibility(View.GONE);
			minus3.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus3:
			llfluidlevels.setVisibility(View.GONE);
			plus3.setVisibility(View.VISIBLE);
			minus3.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus4:
			llsuspension.setVisibility(View.VISIBLE);
			plus4.setVisibility(View.GONE);
			minus4.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus4:
			llsuspension.setVisibility(View.GONE);
			plus4.setVisibility(View.VISIBLE);
			minus4.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus5:
			llengineperformance.setVisibility(View.VISIBLE);
			plus5.setVisibility(View.GONE);
			minus5.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus5:
			llengineperformance.setVisibility(View.GONE);
			plus5.setVisibility(View.VISIBLE);
			minus5.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus6:
			llbrakesystems.setVisibility(View.VISIBLE);
			plus6.setVisibility(View.GONE);
			minus6.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus6:
			llbrakesystems.setVisibility(View.GONE);
			plus6.setVisibility(View.VISIBLE);
			minus6.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus7:
			lloperationalaccessories.setVisibility(View.VISIBLE);
			plus7.setVisibility(View.GONE);
			minus7.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus7:
			lloperationalaccessories.setVisibility(View.GONE);
			plus7.setVisibility(View.VISIBLE);
			minus7.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus8:
			lladdaimage.setVisibility(View.VISIBLE);
			plus8.setVisibility(View.GONE);
			minus8.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus8:
			lladdaimage.setVisibility(View.GONE);
			plus8.setVisibility(View.VISIBLE);
			minus8.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus9:
			llcoverpagelogo.setVisibility(View.VISIBLE);
			plus9.setVisibility(View.GONE);
			minus9.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus9:
			llcoverpagelogo.setVisibility(View.GONE);
			plus9.setVisibility(View.VISIBLE);
			minus9.setVisibility(View.GONE);
			break;

		}

	}

	private void check_validation() {

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent intent = new Intent(VehicleInspection.this, HomeScreen.class);
		startActivity(intent);
		finish();
	}

}
